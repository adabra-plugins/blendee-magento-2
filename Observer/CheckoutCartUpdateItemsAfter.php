<?php

/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @copyright  Copyright (c) 2017 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Blendee\Connector\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CheckoutCartUpdateItemsAfter extends AbstractTrackingObserver implements ObserverInterface
{
     /**
     * @param Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->isEnabled()) {
            return;
        }

        if ($this->isTrackingCartDisable()) {
            return;
        }
        $cart = $observer->getEvent()->getCart();
        foreach ($cart->getQuote()->getAllItems() as $item) {
            $product = $item->getProduct();
            $newQty = $item->getQty();
            $oldQty = $item->getOrigData('qty');
            if(!isset($oldQty)) return;
            $oldQty = intVal($oldQty);
            if ($newQty != $oldQty) {
                if($newQty > $oldQty) $this->tracking->trackAddToCart($product, $newQty - $oldQty);
                else $this->tracking->trackRemoveFromCart($item, $oldQty - $newQty);
            }
        }
    }
}