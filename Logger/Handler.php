<?php

namespace Blendee\Connector\Logger;
use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $loggerType = Logger::WARNING;

    protected $fileName = '/var/log/blendee.log';
}