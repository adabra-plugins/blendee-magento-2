<?xml version="1.0"?>
<!--
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Blendee
 * @package    Blendee_Connector
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_file.xsd">
    <system>
        <tab id="blendee_connector" translate="label" sortOrder="200" class="blendee-logo">
            <label>Blendee</label>
        </tab>

        <section id="adabra_feed" showInDefault="1" showInWebsite="0" showInStore="0">
            <class>separator-top</class>
            <label>Blendee Feed</label>
            <tab>blendee_connector</tab>
            <resource>Blendee_Connector::config</resource>

            <group id="general" translate="label" type="text" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>General</label>
                <field id="version" translate="label" type="label" sortOrder="1" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Current Version</label>
                    <backend_model>Blendee\Connector\Model\Config\Version</backend_model>
                </field>
                <field id="use_cron" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Use Magento Cron</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <backend_model>Blendee\Connector\Model\Config\UseCron</backend_model>
                    <comment><![CDATA[
                        If you set this value to "No", you will have to manually configure your crontab
                        using the bin/magento script.
                    ]]></comment>
                </field>
                <field id="compress" translate="label comment" type="select" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Apply GZIP compression</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="rebuild_time" translate="label comment" type="multiselect" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Rebuild time</label>
                    <source_model>Blendee\Connector\Model\Source\RebuildTime</source_model>
                </field>
                <field id="base_media_url" translate="label" type="select" sortOrder="40" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Base media url</label>
                    <source_model>\Blendee\Connector\Model\Source\MediaPath</source_model>
                </field>
                <field id="incremental_enabled" translate="label comment" type="select" sortOrder="50" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Enable Incremental Customer and Subscriber Feed</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="api_notify" translate="label comment" type="select" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Send Api notification after feed ending</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="api_staging_mode" translate="label comment" type="select" sortOrder="70" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Set Api notification staging mode</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment>Enable Api call staging mode</comment>
                    <depends>
                        <field id="api_notify">1</field>
                    </depends>
                </field>
                <field id="logging_mode_enabled" translate="label comment" type="select" sortOrder="80" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Enable Logging mode</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment>Enable log to specific file in var/log/blendee.log</comment>
                </field>
            </group>

            <group id="http" translate="label" type="text" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>HTTP Download</label>
                <field id="enabled" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>HTTP Download Enabled</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="user" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Auth user</label>
                </field>
                <field id="pass" translate="label comment" type="password" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Auth pass</label>
                </field>
            </group>

            <group id="ftp" translate="label" type="text" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>FTP Upload</label>
                <field id="enabled" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Upload via FTP</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="user" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>FTP user</label>
                </field>
                <field id="pass" translate="label comment" type="password" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>FTP pass</label>
                </field>
                <field id="host" translate="label comment" type="text" sortOrder="40" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>FTP host</label>
                </field>
                <field id="port" translate="label comment" type="text" sortOrder="50" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>FTP port</label>
                </field>
                <field id="path" translate="label comment" type="text" sortOrder="60" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>FTP path</label>
                </field>
                <field id="ssl" translate="label comment" type="select" sortOrder="70" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>FTP SSL mode</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="passive" translate="label comment" type="select" sortOrder="80" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>FTP Passive mode</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
            </group>

            <group id="ftp_ssh" translate="label" type="text" sortOrder="35" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>sFTP (FTP over ssh)</label>
                <field id="enabled" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Upload via sFTP</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="user" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>sFTP user</label>
                </field>
                <field id="public_key" translate="label comment" type="textarea" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>sFTP private key</label>
                </field>
                <field id="host" translate="label comment" type="text" sortOrder="40" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>sFTP host</label>
                </field>
                <field id="port" translate="label comment" type="text" sortOrder="50" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>sFTP port</label>
                </field>
                <field id="path" translate="label comment" type="text" sortOrder="60" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>sFTP path</label>
                </field>
            </group>

            <group id="order" translate="label" type="text" sortOrder="40" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>Order Export</label>
                <field id="states" translate="label comment" type="multiselect" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Export States</label>
                    <source_model>Blendee\Connector\Model\Source\OrderStates</source_model>
                </field>
                <field id="days_interval" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Export Days Interval</label>
                    <source_model>Blendee\Connector\Model\Source\OrderDaysInterval</source_model>
                </field>
                <field id="exclude_order" translate="label comment" type="select" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Exclude order using prefix or postfix regular expression</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="filter_list" translate="label comment" type="textarea" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Filter expression</label>
                    <comment>List of expression for filtering (use | as separator)</comment>
                    <depends>
                        <field id="exclude_order">1</field>
                    </depends>
                </field>
            </group>

            <group id="products" translate="label" type="text" sortOrder="50" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>Product Export</label>
                <field id="enable_custom_short_description" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Enable custom short description for Product Feed</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="custom_short_description" translate="label comment" type="textarea" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Custom expression</label>
                    <comment>
                        <![CDATA[
                            Insert Php code (without using php opening and closing tags) to calculate custom short description for products export.
                            Use <b>$adbProductShortDescription</b> as variable where assign your short description.<br />
                            ex: <br/>
                            <br/>
                            <code>$adbProductShortDescription = $product->getData('custom_attribute_1'). ' ' . $product->getData('custom_attribute_2');</code><br>
                            <br/>
                            Be careful using this option.
                        ]]>
                    </comment>
                    <depends>
                        <field id="enable_custom_short_description">1</field>
                    </depends>
                </field>
            </group>

            <group id="customer" translate="label" type="text" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="0">
                <label>Customer Feed Export</label>
                <field id="export" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Set Customer Feed Export</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="address_type" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Choose type of address to export</label>
                    <source_model>Blendee\Connector\Model\Source\CustomerAddressType</source_model>
                </field>
            </group>

            <group id="newsletter" translate="label" type="text" sortOrder="70" showInDefault="1" showInWebsite="1" showInStore="0">
                <label>Newsletter Feed Export</label>
                <field id="export" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Set Newletter Export</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
            </group>

            <group id="batch_size" translate="label" type="text" sortOrder="80" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>Export batch sizes</label>
                <field id="product" translate="label comment" type="text" sortOrder="10" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Products batch size</label>
                    <comment>Set 0 to disable</comment>
                </field>
                <field id="category" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Categories batch size</label>
                    <comment>Set 0 to disable</comment>
                </field>
                <field id="customer" translate="label comment" type="text" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Customers batch size</label>
                    <comment>Set 0 to disable</comment>
                </field>
                <field id="order" translate="label comment" type="text" sortOrder="40" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Orders batch size</label>
                    <comment>Set 0 to disable</comment>
                </field>
            </group>

            <group id="tags" translate="label" type="text" sortOrder="90" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>Tags</label>
                <field id="tags_list" translate="label comment" type="textarea" sortOrder="21" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Tags List</label>
                    <comment>List of attribute (one per row, use admin value)</comment>
                </field>
            </group>

        </section>

        <section id="adabra_tracking" showInDefault="1" showInWebsite="1" showInStore="1" sortOrder="4">
            <label>Blendee Tracking</label>
            <tab>blendee_connector</tab>
            <resource>Blendee_Connector::config</resource>

            <group id="general" translate="label" type="text" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>General</label>
                <field id="version" translate="label" type="label" sortOrder="1" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Current Version</label>
                    <backend_model>Blendee\Connector\Model\Config\Version</backend_model>
                </field>
                <field id="enabled" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Enabled</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="tracking_url" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Tracking url</label>
                    <comment>Provided by Blendee, use s-t.blendee.com for testing</comment>
                </field>
                <field id="site_id" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Site id</label>
                    <comment>Provided by Blendee</comment>
                </field>
                <field id="catalog_id" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Catalog id</label>
                    <comment>Provided by Blendee</comment>
                </field>
                <field id="disable_tracking_cart_actions" translate="label" type="select" sortOrder="30" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Disable tracking for add to cart and remove from cart actions</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
            </group>
        </section>
    </system>
</config>
