# Blendee Plugin for Magento 2

Il modulo integra i sistemi basati su Magento 2 con Blendee

## Installazione modulo o aggiornamento
Il modulo va installato/aggiornato tramite i seguenti comandi:
```
composer require blendee/blendee-magento2
```
```
bin/magento setup:upgrade
```
Se la tua installazione di Magento lavora in ambiente di produzione esegui anche:
```
bin/magento setup:static-content:deploy -f
```
Infine svuotare la cache di sistema con:
```
php bin/magento cache:clean && php bin/magento cache:flush
```

## Cron job
Gli script eseguiti per effettuare gli import sono:
* Command/Run.php: esporta i feed con status = STATUS_MARKED_REBUILD ('marked-rebuild')
* Command/Rebuild.php: imposta lo stato dei feed a STATUS_MARKED_REBUILD ('marked-rebuild') per rieseguire Command/Run.php

Il modulo permette di gestire i task per l'import delle risorse verso blendee in 2 modalità:

* scheduling gestito interamente da Magento: installare il crontab di magento con     
  ```
  php bin/magento cron:install
  ```
  Magento andrà ad inserire all'interno del crontab un comando racchiuso tra:
    ```
    #~ MAGENTO START
    ....
    #~ MAGENTO END
    ```
  che ad ogni minuto controllerà i job da eseguire
* scheduling gestito manualmente andando a modificare il crontab. Dopo aver disattivato i cron di Magento in "Stores > Configuration > Blendee > Feed > General" aggiungere al crontab:
    ```
    bin/magento blendee:feed:run
    bin/magento blendee:feed:rebuild
    ```
    Dato che il comando blendee:feed:run esegue l'export dei feed con status STATUS_MARKED_REBUILD ('marked-rebuild') si consiglia di richiamarlo con una frequenza alta (si consiglia una frequenza di una chiamata al minuto), mentre blendee:feed:rebuild dovrà essere richiamato solo nel momento in cui si vuole avviare la ricostruzione del feed.

In caso di passaggio dai cron Magento a quelli manuali i job verranno disattivati come da documentazione, impostando una data non eseguibile (30 febbraio)
## Configurazione
Per maggiori dettagli sulla configurazione del modulo lato frontend fare riferimento alla seguente documentazione
https://drive.google.com/file/d/1pbpZxbzRkzZfkKwJF2PeZR1cCoJeWlII/view