<?php
/**
 * Created by PhpStorm.
 * User: matteo
 * Date: 2019-01-31
 * Time: 18:06
 */

namespace Blendee\Connector\Controller\Adminhtml\Feed;
use Blendee\Connector\Api\FeedManagerInterface;
use \Magento\Framework\Message\ManagerInterface;


use \Magento\Framework\App\Action\Action as controllerAction;
class ResetIncremental extends controllerAction {

    /**
     * @var FeedManagerInterface
     */
    protected $feedManagerInterface;

    /**
     * @var
     */
    protected $messageManager;



    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        FeedManagerInterface $feedManagerInterface,
        ManagerInterface $messageManager
    ) {
        $this->feedManagerInterface = $feedManagerInterface;
        $this->messageManager = $messageManager;

        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Blendee_Connector::blendee_feed_reset_incremental');
    }
    /**
     * Sets the content of the response
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $this->feedManagerInterface->resetIncremental();
        $this->messageManager->addSuccessMessage(__("Reset feed correctly done"));
        return $resultRedirect->setPath('*/*/');

    }
}
