<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Blendee
 * @package    Blendee_Connector
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Blendee\Connector\Controller\Adminhtml\Feed;

use Blendee\Connector\Api\Data\SubFeedInterface;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;

class Save extends Action
{
    protected $pageFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory
    ) {
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Blendee\Connector\Model\Feed $model */
            $model = $this->_objectManager->create('Blendee\Connector\Model\Feed');

            $id = $this->getRequest()->getParam('adabra_feed_id');

            if ($id) {
                $model->load($id);
                $model->setData($data);
                $model->setData('currency', $model->getCurrencyCode());
                $model->setData('updated_at', (new \DateTime())->format('Y-m-d H:i:s'));
            } else {
                array_splice($data, 0, 1);

                $model->setData($data);
                $model->setData('status_order', SubFeedInterface::STATUS_MARKED_REBUILD);
                $model->setData('status_product', SubFeedInterface::STATUS_MARKED_REBUILD);
                $model->setData('status_category', SubFeedInterface::STATUS_MARKED_REBUILD);
                $model->setData('status_customer', SubFeedInterface::STATUS_MARKED_REBUILD);
                $model->setData('status_subscriber', SubFeedInterface::STATUS_DISABLE);
                $model->setData('updated_at', (new \DateTime())->format('Y-m-d H:i:s'));
            }

            $this->_eventManager->dispatch(
                'feed_post_prepare_save',
                ['feed' => $model, 'request' => $this->getRequest()]
            );

            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved this Feed.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['post_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the feed.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['adabra_feed_id' => $this->getRequest()->getParam('adabra_feed_id')]);
        }
        return $resultRedirect->setPath('*/*/');

        /** @var \Magento\Backend\Model\View\Result\Page $result */
        $result = $this->pageFactory->create();
        return $result;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Blendee_Connector::blendee_feed_save');
    }
}
