<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @copyright  Copyright (c) 2017 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Blendee\Connector\Model\Tracking;

use Magento\Framework\Stdlib\Cookie\CookieMetadata;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Laminas\Json\Json;

class Storage implements StorageInterface
{

    const COOKIE_NAME = 'adabra_actions';
    const QUEUE_NAME = 'queue';
    private $queue_copy = [];
    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;
    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    public function __construct(
        CookieMetadataFactory $cookieMetadataFactory,
        CookieManagerInterface $cookieManager
    ) {
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieManager = $cookieManager;
    }

    /**
     * @param string $action
     * @param array $params
     * @return $this
     */
    public function addToQueue($action, $params = [], $actionNumber=1)
    {
        $queue = $this->getQueue();

        if (count($params) == count($params, COUNT_RECURSIVE)) {
            $queue[] = compact("action", "params", "actionNumber");
        } else {
            foreach($params as $singleAction) {
                $queue[] = array("action" => $action, "params" => $singleAction, 'actionNumber' => $actionNumber);
            }
        }
        /* comprimo il cookie adabra_action (deflate + base64_encode) e verifico che abbia lunghezza inferiore a 4096 kb */
        // TODO: gestione cookie maggiore di 4096 kb

        $this->queue_copy = $queue;

        $deflatedQueue = base64_encode(gzdeflate(Json::encode($queue),9, ZLIB_ENCODING_DEFLATE));
        if(strlen($deflatedQueue) <= 4096) {
            $this->cookieManager->setPublicCookie(static::COOKIE_NAME, $deflatedQueue, $this->getMetadata());

        }
        return $this;
    }

    /**
     * @param bool $clear
     * @return array
     */
    public function getQueue($clear = false)
    {

        $queue = $this->cookieManager->getCookie(static::COOKIE_NAME);
        
        if (!$queue && $this->queue_copy) return $this->queue_copy;

        if ($clear) {
            $this->cookieManager->setPublicCookie(static::COOKIE_NAME, Json::encode([]), $this->getMetadata());
        }

        if (empty($queue)) {
            $queue = [];
        } else {
            /* verifico che la coda sia correttamente encodata in base64 */
            if (Json::decode(gzuncompress(base64_decode($queue,true)))) {
                /* decompressione cookie (base64_decode + inflate) */
                $queue = Json::decode(gzuncompress(base64_decode($queue)));
            }
            else {
                $queue = [];
            }
        }
        return $queue;
    }


    /**
     * @return $this
     */
    public function flushAll()
    {
        $this->cookieManager->deleteCookie(static::COOKIE_NAME);

        return $this;
    }

    /**
     * @return \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata
     */
    protected function getMetadata()
    {
        return $this->cookieMetadataFactory->createPublicCookieMetadata([CookieMetadata::KEY_PATH => '/']);
    }
}
