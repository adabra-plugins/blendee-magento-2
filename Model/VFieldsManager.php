<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Blendee
 * @package    Blendee_Connector
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Blendee\Connector\Model;

use Blendee\Connector\Api\Data\VFieldsInterface;
use Blendee\Connector\Api\VFieldsManagerInterface;
use Blendee\Connector\Helper\Filesystem;
use Blendee\Connector\Model\ResourceModel\VFields\Collection;
use Blendee\Connector\Model\ResourceModel\VFields\CollectionFactory as VFieldsCollectionFactory;

class VFieldsManager implements VFieldsManagerInterface
{
    protected $filesystem;
    protected $vfieldsCollectionFactory;
    protected $vfields;
    protected $dataHelper;

    protected $vfieldsCollection = null;

    public function __construct(
        Filesystem $filesystem,
        VFieldsCollectionFactory $vfieldsCollectionFactory,
        VFieldsInterface $vfields
    ) {
        $this->filesystem = $filesystem;
        $this->vfieldsCollectionFactory = $vfieldsCollectionFactory;
        $this->vfields = $vfields;
    }

    /**
     * Check if vfield has a magento attribute mapped
     * @param $code
     * @return Collection
     */
    public function checkVFieldToExport($code)
    {

        $this->vfieldsCollection = $this->vfieldsCollectionFactory->create();
        $this->vfieldsCollection->filterToBuild($code);
        return $this->vfieldsCollection;
    }
}
