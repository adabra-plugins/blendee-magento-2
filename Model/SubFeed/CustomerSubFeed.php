<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Blendee
 * @package    Blendee_Connector
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Blendee\Connector\Model\SubFeed;

use Blendee\Connector\Api\Data\SubFeedInterface;
use Blendee\Connector\Helper\Data as DataHelper;
use Blendee\Connector\Helper\Ftp as FtpHelper;
use Blendee\Connector\Helper\Filesystem;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\ResourceConnection;
use Magento\Directory\Helper\Data as DirectoryHelperData;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\UrlInterface;
use Magento\Newsletter\Model\Subscriber;
use Magento\Customer\Model\Address\AbstractAddress;


class CustomerSubFeed extends AbstractSubFeed implements SubFeedInterface
{
    protected $type = 'customer';
    protected $scope = 'website';
    protected $exportName = 'customers';
    protected $collectionFactory;
    protected $subscriber;
    protected $resourceConnection;
    protected $connection;
    protected $subscriberRepositoryInterface;
    protected $frontendUrlBuilder;

    /**
     * CustomerSubFeed constructor.
     * @param File $file
     * @param Csv $csv
     * @param Filesystem $filesystem
     * @param DataHelper $dataHelper
     * @param FtpHelper $ftpHelper
     * @param DirectoryHelperData $directoryHelperData
     * @param DateTime $dateTime
     * @param CollectionFactory $collectionFactory
     * @param Subscriber $subscriber
     * @param AbstractAddress $abstractAddress
     * @param ResourceConnection $resourceConnection
     * @param UrlInterface $frontendUrlBuilder
     */
    public function __construct(
        File $file,
        Csv $csv,
        Filesystem $filesystem,
        DataHelper $dataHelper,
        FtpHelper $ftpHelper,
        DirectoryHelperData $directoryHelperData,
        DateTime $dateTime,
        CollectionFactory $collectionFactory,
        Subscriber $subscriber,
        AbstractAddress $abstractAddress,
        ResourceConnection $resourceConnection,
        UrlInterface $frontendUrlBuilder

    ) {
        parent::__construct($file, $csv, $filesystem, $dataHelper, $ftpHelper, $directoryHelperData, $dateTime);

        $this->collectionFactory = $collectionFactory;
        $this->subscriber = $subscriber;
        $this->abstractAddress = $abstractAddress;
        $this->dataHelper = $dataHelper;
        $this->resourceConnection = $resourceConnection;
        $this->frontendUrlBuilder = $frontendUrlBuilder;

    }

    /**
     * Get headers
     * @return array
     */
    protected function getHeaders()
    {
        return [
            'id_utente',
            'email',
            'nome',
            'cognome',
            'citta',
            'cap',
            'indirizzo',
            'provincia',
            'regione',
            'stato',
            'cellulare',
            'telefono',
            'sesso',
            'nascita_anno',
            'nascita_mese',
            'nascita_giorno',
            'f_business',
            'azienda_nome',
            'azienda_categoria',
            'f_ricevi_newsletter',
            'f_ricevi_newsletter_ts',
            'f_ricevi_comunicazioni_commerciali',
            'data_iscrizione',
            'data_cancellazione',
            'ip',
            'user_agent',
            'f_attivo',
            'f_cancellato',
            'unsubscribe_code'
        ];
    }

    /**
     * Prepare feed collection
     * @return void
     */
    protected function prepareCollection()
    {
        $this->collection = $this->collectionFactory->create();
        $this->collection
            ->addAttributeToSelect('*')
            ->addFieldToFilter('email', array ('nlike'=>'%@marketplace.amazon%'))
            ->addFieldToFilter('website_id', $this->getFeed()->getWebsite()->getId());
        if($this->dataHelper->isIncrementalEnabled()) {
            $customerFeedLastUpdate = $this->getFeed()->getFeedLastUpdate($this->type);
            if (isset($customerFeedLastUpdate)){
                /**
                 * controllo se il cliente ha aggiornato i dati anagrafici (customer_entity) oppure un indirizzo (customer_address_entity)
                 * oppure i dati relativi alle subscription newsletter (newsletter_subscriber)
                 */
                $customerAddressEntity = $this->resourceConnection->getTableName('customer_address_entity');
                $newsletterSubscriber = $this->resourceConnection->getTableName('newsletter_subscriber');
                $addressSubquery = $this->resourceConnection->getConnection()->select()
                    ->from(
                        ['ca' => $customerAddressEntity], 
                        ['parent_id', 'updated_at' => 'MAX(ca.updated_at)']
                    )
                    ->where('ca.updated_at > ?', $customerFeedLastUpdate)
                    ->group('ca.parent_id');
                $subscriberSubquery = $this->resourceConnection->getConnection()->select()
                    ->from(
                        ['ns' => $newsletterSubscriber], 
                        ['customer_id', 'change_status_at' => 'MAX(ns.change_status_at)']
                    )
                    ->where('ns.change_status_at > ?', $customerFeedLastUpdate)
                    ->group('ns.customer_id');
                $this->collection
                    ->getSelect()
                    ->joinLeft(['customer_address' => $addressSubquery],'customer_address.parent_id = e.entity_id', ['address_updated_at' => 'customer_address.updated_at'])
                    ->joinLeft(['customer_subscriber' => $subscriberSubquery], 'customer_subscriber.customer_id = e.entity_id', ['subscriber_updated_at' => 'customer_subscriber.change_status_at'])
                    ->where('(e.updated_at > ? OR customer_address.updated_at is not NULL OR customer_subscriber.change_status_at is not NULL)', $customerFeedLastUpdate);
            }
        }
    }

    /**
     * Get virtual field value
     * @param Customer $customer
     * @param $field
     * @return string
     */
    public function getVirtualField(Customer $customer, $field)
    {
        // TODO: Virtual fields mapping
        return $customer->getData($field);
    }

    /**
     * Check if a customer has a newsletter subscription
     * @param Customer $customer
     * @return bool
     */
    protected function getIsInNewsletterSubscriberTable(Customer $customer)
    {
        $storeList = array();
        foreach ($this->getFeed()->getStore()->getWebsite()->getGroups() as $group) {
            $stores = $group->getStores();
            foreach ($stores as $store) {
                array_push($storeList,  $store->getStoreId());
            }
        }
        if ($customer->getId()) {
            $this->connection = $this->resourceConnection;
            $tableName = $this->connection->getTableName('newsletter_subscriber'); // It will return table with prefix
            $select = $this->connection->getConnection()
                ->select()
                ->from($tableName)
                ->where('customer_id = ?', $customer->getId())
                ->where('store_id IN (?)', $storeList)
                ->limit(1);

            $result = $this->connection->getConnection()->fetchRow($select);

            return $result ? $result : false;
        }
    }

    /**
     * Build unsubscription link newsletter subscription
     * @param $subscriberId
     * @param $subscriberCode
     * @return string
     */
    protected function getUnsubscribeLinkNewsletterSubscriber($subscriberId, $subscriberCode)
    {
        return $this->frontendUrlBuilder->setScope(
            $this->getFeed()->getStore()
        )->getUrl(
            'newsletter/subscriber/unsubscribe',
            ['id' => $subscriberId, 'code' => $subscriberCode, '_nosid' => true]
        );
    }

    /**
     * Get feed row for entity
     * @param $entity
     * @return array
     */
    protected function getFeedRow($entity)
    {
        /** @var $customer Customer */
        $customer = $entity;

        $addressType = $this->dataHelper->getCustomerAddressType();
        if ($addressType == $this->abstractAddress::TYPE_BILLING) {
            $address = $customer->getDefaultBillingAddress();
        } else if ($addressType == $this->abstractAddress::TYPE_SHIPPING) {
            $address = $customer->getDefaultShippingAddress();
        }

        if ($customer->getDob()) {
            $dob = preg_split('/\D+/', $customer->getDob());
        } else {
            $dob = ['', '', ''];
        }
        $isInNewsletterTable = $this->getIsInNewsletterSubscriberTable($customer);
        $subscriptionStatus = false;
        if ($isInNewsletterTable && $isInNewsletterTable['subscriber_id']) {
            $subscriptionStatus = $isInNewsletterTable['subscriber_status'];
            if ($subscriptionStatus == Subscriber::STATUS_SUBSCRIBED) {
                $subscriptionStatus = true;
            } else if ($subscriptionStatus == Subscriber::STATUS_UNSUBSCRIBED) {
                $subscriptionStatus = false;
            }
        }
        return [[
            $customer->getId(),
            $customer->getEmail(),
            $customer && $customer->getFirstname() ? $this->dataHelper->sanitizeString($customer->getFirstname()) : '',
            $customer && $customer->getLastname() ? $this->dataHelper->sanitizeString($customer->getLastname()) : '',
            $address ? $address->getCity() : '',
            $address ? $address->getPostcode() : '',
            $address && $address->getStreetFull() ? $this->dataHelper->sanitizeString($address->getStreetFull()) : '',
            '',
            $address ? $address->getRegionCode() : '',
            $address ? $address->getCountry() : '',
            '',
            $address ? $address->getTelephone() : '',
            $customer->getGender() == 2 ? 'f' : 'm',
            $dob[0],
            $dob[1],
            $dob[2],
            $this->getVirtualField($customer, 'f_business'),
            $address ? $address->getCompany() : '',
            $this->getVirtualField($customer, 'azienda_categoria'),
            $subscriptionStatus ? $this->toBoolean(true) : $this->toBoolean(false),
            isset($isInNewsletterTable['change_status_at']) ? $isInNewsletterTable['change_status_at'] : '',
            $this->toBoolean($this->getVirtualField($customer, 'f_ricevi_comunicazioni_commerciali')),
            $this->toTimestamp2($customer->getCreatedAtTimestamp()),
            '',
            '',
            '',
            $this->toBoolean(true),
            $this->toBoolean(false),
            $subscriptionStatus ? $this->getUnsubscribeLinkNewsletterSubscriber($isInNewsletterTable['subscriber_id'], $isInNewsletterTable['subscriber_confirm_code']) : ''
        ]];
    }
}
