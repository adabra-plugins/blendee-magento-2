<?php
/**
 * Created by PhpStorm.
 * User: matteo
 * Date: 2019-01-07
 * Time: 12:01
 */

namespace Blendee\Connector\Model\Source;

use Magento\Customer\Model\Address\AbstractAddress;

class CustomerAddressType extends SourceAbstract
{
    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => AbstractAddress::TYPE_SHIPPING,
                'label' => __('Shipping')
            ], [
                'value' => AbstractAddress::TYPE_BILLING,
                'label' => __('Billing')
            ]
        ];
    }
}