<?php
namespace Blendee\Connector\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

class VFieldsMode extends SourceAbstract implements SourceInterface, OptionSourceInterface
{
    const MODE_MAP = 'map';
    const MODE_STATIC = 'static';
    public function toOptionArray()
    {
        return [
            ['value' => self::MODE_MAP, 'label' => 'Map to Attribute'],
            ['value' => self::MODE_STATIC, 'label' => 'Static Value']
        ];
    }
}
