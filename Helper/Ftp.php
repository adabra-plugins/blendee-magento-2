<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Blendee
 * @package    Blendee_Connector
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Blendee\Connector\Helper;

use Blendee\Connector\Helper\Data as DataHelper;
use Magento\Framework\Filesystem\Io\FtpFactory;
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;

class Ftp
{
    protected $data;
    protected $ftpFactory;
    protected $dataHelper;

    public function __construct(
        Data $data,
        FtpFactory $ftpFactory,
        DataHelper $dataHelper
    ) {
        $this->data = $data;
        $this->ftpFactory = $ftpFactory;
        $this->dataHelper = $dataHelper;
    }

    /**
     * FTP upload
     * @param $localFileName
     * @param $remoteFileName
     */
    public function uploadFile($localFileName, $remoteFileName)
    {
        $ftp = $this->ftpFactory->create();
        $transferMode = (strpos($remoteFileName, '.gz') === false) ? FTP_ASCII : FTP_BINARY;
        try {
            $ftp->open([
                'host' => $this->data->getFtpHost(),
                'port' => $this->data->getFtpPort(),
                'user' => $this->data->getFtpUser(),
                'password' => $this->data->getFtpPass(),
                'ssl' => $this->data->getFtpSsl(),
                'passive' => $this->data->getFtpPassive(),
                'path' => $this->data->getFtpPath(),
                'file_mode' => $transferMode,
            ]);
            $ftp->write($remoteFileName, $localFileName, $transferMode);
        } catch (\Exception $e) {
            $this->dataHelper->logError($e);
        }

        $ftp->close();
    }

    /**
     * FTP ssh upload
     * @param $localFileName
     * @param $remoteFileName
     * @param $rsaKey
     */
    public function uploadSshFile($localFileName, $remoteFileName, $rsaKey): void
    {
        $key = new RSA();
        try {
            $key->loadKey($rsaKey);
        } catch (\Exception $e) {
            $this->dataHelper->logError($e);
        }

        $sftp = new SFTP($this->data->getSshFtpHost());
        try {
            $sftp->login($this->data->getSshFtpUser(), $key);
        } catch (\Exception $e) {
            $this->dataHelper->logError($e);
        }

        try {
            $sftp->put(
                $remoteFileName,
                $localFileName,
                SFTP::SOURCE_LOCAL_FILE
            );
        } catch (\Exception $e) {
            $this->dataHelper->logError($e);
        }

        $sftp->disconnect();
    }
}
