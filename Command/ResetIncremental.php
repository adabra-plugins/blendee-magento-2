<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Blendee
 * @package    Blendee_Connector
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Blendee\Connector\Command;

use Blendee\Connector\Api\FeedManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetIncremental extends Command
{
    protected $feedManager;

    public function __construct(
        FeedManagerInterface $feedManager
    ) {
        parent::__construct();

        $this->feedManager = $feedManager;
    }

    protected function configure()
    {
        $this->setName('blendee:feed:reset-incremental');
        $this->setDescription('Reset feed incremental date');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->feedManager->resetIncremental();
        return 0;
    }
}
